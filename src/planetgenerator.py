import random
from tkinter import *
from PIL import Image, ImageTk
import time
import os


root = Tk()
root.title('Planet Generator') # Title
root.geometry('1200x800') # Window size
root.resizable(False, False) # Not resizable


def generate():
    # If user clicks the button - redirects here & generate new
    pic_pick() # image
    name_gen() # name
    temperature() # temperature
    size_gen() # radius


def pic_pick():
    # Define random it as string var - open that random image & sets it as background
    pic_choice = random.randint(1, 20)
    grab = f'{pic_choice}.jpeg'
    background_img = Image.open(f'/home/commrat/StarCore/Planets/{grab}')
    img_bck = background_img.resize((1200, 800), Image.ANTIALIAS)
    background = ImageTk.PhotoImage(img_bck)

    lb.configure(image=background)
    lb.image = background


def name_gen():
    # Generates new planet name in format AA0000000
    name_ls = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'P']
    name_ls2 = ['R', 'Z', 'Y', 'Q', 'S', 'U']
    name_ls3 = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

    nl1 = random.choice(name_ls)
    nl2 = random.choice(name_ls2)

    new_ls = []
    x = 0
    while x < 7:
        y = random.choice(name_ls3)
        new_ls.append(y)
        x += 1

    nums = ''

    for ele in new_ls:
        nums += ele

    new_name = 'Name: ' + nl1 + nl2 + nums

    name = Label(root, text=f'{new_name}')
    name.place(x=0, y=10)


def temperature():
    # Creates new temperature
    min_temp = -41
    max_temp = 63

    avg_temp = random.randint(min_temp, max_temp)

    temp = Label(root, text=f'Avg. Temperature: {avg_temp} °C ')
    temp.place(x=0, y=25)


def size_gen():
    # Generates new planet's radius
    earth_size = 12756
    size_min = (earth_size - 5000) / 2
    size_max = (earth_size + 20000) / 2

    new_size = random.randint(size_min, size_max)

    rad = Label(root, text=f'Radius: {new_size} km ')
    rad.place(x=0, y=40)

# Set a welcome screen with first planet

lb = Label(root)
lb.pack()

gen_btn = Button(root, text='Generate new planet', command=generate)
gen_btn.place(x=500, y=10)

name = Label(root, text='Name: TR88452698')
name.place(x=0, y=10)

temp = Label(root, text='Avg. Temperature: 21 °C  ')
temp.place(x=0, y=25)

rad = Label(root, text='Radius: 7412 km ')
rad.place(x=0, y=40)

background_img = Image.open("/home/commrat/StarCore/Planets/9.jpeg")
img_bck = background_img.resize((1200, 800), Image.ANTIALIAS)
background = ImageTk.PhotoImage(img_bck)

lb.configure(image=background)
lb.image = background

root.mainloop()
