## Planet generator
Planet generator is simple program, which generates random values that 
are presented as:
1) Planet name
2) Average temperature (°C)
3) Radius of the planet (km)
4) Planet picture 
All those parameters are randomly generated.

## Developement
If you try to run that on your computer, you have to download the 
'Planets' folder and in the 'planetGen.py' script, you have to change
location of those pictures - in line 24 & 95

## Requirements
- Python 3+
- tkinter 8.6
- PIL 8.1.2

## License
Public domain