#!/usr/bin/python

from setuptools import setup
from src.version import __version__

setup(
    name='planetgenerator',
    version=__version__,
    description='Exploring random planets.',
    # long_description='',
    # packages=['src'],
    # install_requires=[],
    package_dir={'':'src'},
    entry_points={"console_scripts": ["planetgenerator = planetgenerator"]},
)
